#karate
wget -nc http://networkdata.ics.uci.edu/data/karate/karate.gml
wget -nc http://networkdata.ics.uci.edu/data/karate/karate.txt
mkdir -p karate
mv karate.gml karate
mv karate.txt karate/karate_groups.txt
python convert-gml.py karate/karate.gml karate/karate_edges.txt karate/karate_truth_bad_do_not_use.txt 1

#football
wget -nc http://networkdata.ics.uci.edu/data/football/football.gml
wget -nc http://networkdata.ics.uci.edu/data/football/football.txt
mkdir -p football
mv football.gml football
mv football.txt football/football_groups.txt
python convert-gml.py football/football.gml football/football_edges.txt football/football_truth.txt

#polblogs
wget -nc http://networkdata.ics.uci.edu/data/polblogs/polblogs.gml
wget -nc http://networkdata.ics.uci.edu/data/polblogs/polblogs.txt
mkdir -p polblogs
mv polblogs.gml polblogs
mv polblogs.txt polblogs/polblogs_groups.txt
python convert-gml.py polblogs/polblogs.gml polblogs/polblogs_edges.txt polblogs/polblogs_truth.txt 1

#polbooks
wget -nc http://networkdata.ics.uci.edu/data/polbooks/polbooks.gml
wget -nc http://networkdata.ics.uci.edu/data/polbooks/polbooks.txt
mkdir -p polbooks
mv polbooks.gml polbooks
mv polbooks.txt polbooks/polbooks_groups.txt
python convert-gml.py polbooks/polbooks.gml polbooks/polbooks_edges.txt polbooks/polbooks_dontuse.txt

#adjnoun
wget -nc http://networkdata.ics.uci.edu/data/adjnoun/adjnoun.gml
wget -nc http://networkdata.ics.uci.edu/data/adjnoun/adjnoun.txt
mkdir -p adjnoun
mv adjnoun.gml adjnoun
mv adjnoun.txt adjnoun/adjnoun_groups.txt
python convert-gml.py adjnoun/adjnoun.gml adjnoun/adjnoun_edges.txt adjnoun/adjnoun_truth.txt

#dolphins
python convert-gml.py dolphins/dolphins.gml dolphins/dolphins_edges.txt dolphins/dolphins_truth.txt 