import networkx as nx
import sys

if len(sys.argv) < 4:
	print "Usage: %s <input gml> <output edge list> <output membership> [<0 or 1 for index>]" % sys.argv[0]
	quit()

index = 0
if len(sys.argv) > 4:
	index = int(sys.argv[4])

g = nx.read_gml(sys.argv[1])

if index == 1:
	new_g = nx.Graph()
	for i in g.nodes():
		new_g.add_node(i-1, value=g.node[i]['value'])
	for e in g.edges():
		new_e = (e[0]-1, e[1]-1)
		new_g.add_edge(*new_e)
	g = new_g
	nx.write_gml(g, sys.argv[1])	

nx.write_edgelist(g, sys.argv[2], delimiter='\t')

attrs = nx.get_node_attributes(g,'value')

keys = attrs.keys()
sorted(keys)
f = open(sys.argv[3],'w')
for key in keys: f.write(str(attrs[key])+'\n')
f.close()
