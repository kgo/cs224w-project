from sframe import SGraph, SFrame, Vertex, Edge
import sframe

# Download and create a graph from edges
edge_data = SFrame.read_csv( 'http://s3.amazonaws.com/dato-datasets/bond/bond_edges.csv')
g = SGraph().add_edges(edge_data, src_field='src', dst_field='dst')
print(g)
g.save('james_bond')

# Save and reload a graph
ng = sframe.load_sgraph('james_bond')
print(ng)

# Visualize graph
# Note: does not work, no such method?
#g.show(vlabel='id', highlight=['James Bond', 'Moneypenny'], arrows=True)


