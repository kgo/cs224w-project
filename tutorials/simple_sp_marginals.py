# This code computes the marginals for a simple graph
# To be used for comparison with BP

# 4 nodes, 4 factors
AB = [[30,5],[1,10]];
BC = [[100,1],[1,100]];
CD = [[1,100],[100,1]];
DA = [[100,1],[1,100]];

# Past a list of lists with which values to use
# Example: [[0,1],[0,1],[0,1],[0]] computes the marginal for D
def computeMarginal(vals):
	total = 0
	for a in vals[0]:
		for b in vals[1]:
			for c in vals[2]:
				for d in vals[3]:
					total += AB[a][b]*BC[b][c]*CD[c][d]*DA[d][a]
	return total

total = float(computeMarginal([[0,1],[0,1],[0,1],[0,1]]))
d_marg0 = computeMarginal([[0,1],[0,1],[0,1],[0]])
d_marg1 = computeMarginal([[0,1],[0,1],[0,1],[1]])
print(d_marg0)
print(d_marg0/total)
print('---------------------')
print(d_marg1)
print(d_marg1/total)
