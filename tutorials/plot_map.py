import numpy as np
import matplotlib.pyplot as plt
import sys

N = 2000

if len(sys.argv) < 2:
	print("python plot_map.py <edge_list_file> [<membership_file>] ")
	exit()

rearrangement = []
if len(sys.argv) == 3:
	def getMembership(fname):
	    text_file = open(fname, "r")
	    membership = {}
	    count = 0
	    for i,val in enumerate(text_file.read().split()):
	    	if val in membership:
	    		membership[int(val)] = []
	    	membership[int(val)].append(i)
	    	count+=1
	    return membership,count
	membership, count = getMembership(sys.argv[2])
	rearrangement = [0]*count
	N = count
	count = 0
	for k,v in getMembership.iteritems():
		for i in v:
			rearrangement[i] = count
			count+=1

mat = np.zeros((N,N))
f = open(sys.argv[1])
f.readline() # skip header line
for line in f:
	splitted = line.split()
	src_node = int(splitted[0])
	dst_node = int(splitted[1])
	if len(rearrangement) > 0:
		src_node = rearrangement[src_node]
		dst_node = rearrangement[dst_node]
		
	mat[src_node,dst_node] = 1
	mat[dst_node,src_node] = 1

plt.imshow(mat,cmap='Greys')
plt.axis('off')
plt.show()
