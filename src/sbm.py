from __future__ import division
import snap, random, math

# Generate a graph from using the stochastic block model
# Graph is undirected
# Parameters are:
#   z: a list where z[i] is the number of nodes in community i
#   M: a len(z)-by-len(z) matrix (list-in-list) where
#      M[i][j] is the probability that a node in group i is
#      connected to a node in group j
def generateSBMGraph(z,M,outputFile=None):
	graph = snap.TUNGraph.New()
	N = sum(z)
	# Put z into an easier to work with form a vector where v[i] is
	# the community of the i-th node
	v = []
	for i in range(len(z)): v += [i]*z[i]
	for i in xrange(N): graph.AddNode(i)
	for i in xrange(N):
		for j in xrange(i+1,N):
			p = M[v[i]][v[j]]
			if random.random() < p: graph.AddEdge(i,j)
	if outputFile != None:
		snap.SaveEdgeList(graph, outputFile)
	return graph

# Examples
#A = {0:[0,1,2,3,4,5,6,7], 1:[8,9,10,11,12], 2:[13,14,15,16]}
#B = {0:[0,2,3,5,7,8], 1:[14, 4, 9,10,11,12], 2:[13,15,16,1,6]}
#print "NMI: ", NMI(A,B)
#print "rNMI: ", rNMI(A,A)
#generateSBMGraph(4,[0,0,1,1],[[1,0.3],[0.3,1]], 'test.txt')
#generateSBMGraph([1000,1000],[[0.2,0.05],[0.05,0.2]],'2000-node-2c.txt')
if False:
	for i in [10,100,1000,10000]:
		for j in [1,2,5]:
			n = i*j
			if n < 50: continue
			p_in = 20/float(n)
			p_out = 5/float(n)
			generateSBMGraph([int(n/2),int(n/2)],[[p_in,p_out],[p_out,p_in]],'test_sbms/' +str(n)+ '-node-2c.txt')
if True:
	n = 2000
	for i in range(5,21):
		p_in = i/float(n)
		p_out = 5/float(n)
		generateSBMGraph([int(n/2),int(n/2)],[[p_in,p_out],[p_out,p_in]],'other_sbms/' +str(i)+ '.txt')

#generateSBMGraph([100000,100000],[[0.002,0.0005],[0.0005,0.002]],'200000-node-2c-low.txt')
