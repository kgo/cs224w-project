from __future__ import division
import random, math, numpy
import scipy.sparse as sparse
import scipy.sparse.linalg
from sklearn.preprocessing import normalize
from scipy.cluster.vq import kmeans,vq
import sys
from nmi import NMI, rNMI, overlap

import time

def computeNumNodes(edges):
	maxE = 0
	for e in edges:
		maxE = max(maxE,e[0],e[1])
	return maxE + 1

def makeAdjMatrix(edges):
	maxE = computeNumNodes(edges)
	#A = numpy.zeros((maxE,maxE))
	A = sparse.dok_matrix((maxE,maxE), dtype=numpy.float32)
	for e in edges:
		A[e[0],e[1]] = 1
		A[e[1],e[0]] = 1
	return A

def find(a, func):
    return [i for (i, val) in numpy.ndenumerate(a) if func(val)]

def buildBH(lamb, A):
	degrees = [i[0,0] for i in A.sum(1)]
	N = A.shape[0]
	D = sparse.diags([degrees],[0])
	return (lamb*lamb - 1) * sparse.eye(N) - lamb * A + D

def buildBHprime(lamb, A):
	degrees = [i[0,0] for i in A.sum(1)]
	N = A.shape[0]
	D = sparse.diags([degrees],[0])
	return 2*lamb*sparse.eye(N) - A

def iterateSlp (A, tol, maxIter):
	err = 1 
	it = 0
	degrees = A.sum(1)
	sq_deg = [i[0,0]*i[0,0] for i in degrees]
	avg_sq =  sum(sq_deg)/len(degrees)
	avg_deg = sum(degrees)/len(degrees)
	lamb = avg_sq/avg_deg -1 
	lamb = lamb[0,0]
	while err > tol and it < maxIter:
		it+=1
		BH = buildBH(lamb, A)
		BHprime = buildBHprime(lamb, A)
		mu, x = sparse.linalg.eigsh(BH,1, M=BHprime, which='SM')
		err = abs(mu[0])
		lamb = lamb - mu[0]
	return x, lamb

#given A (adjacency matrix), N(# of nodes), q (number of communities), 
def betheHessianCluster(A, N, q):
	MAX_ITER = 500
	TOL = 1e-10
	x, rhoB = iterateSlp(A, 1e-10, 10)
	r = math.sqrt(rhoB)
	maxComm = int(math.floor(math.sqrt(N)))
	BHp = buildBH(r, A)
	BHm = buildBH(-r, A)

	eigenvaluesp, vectorsBHp = sparse.linalg.eigsh(BHp,maxComm, which='SA', maxiter=MAX_ITER, tol = TOL)
	eigenvaluesm, vectorsBHm = sparse.linalg.eigsh(BHm,maxComm, which='SA', maxiter=MAX_ITER, tol = TOL)
	idplus = find(eigenvaluesp, lambda x: x<0)
	idminus = find(eigenvaluesm, lambda x: x<0)
	nCommBH = len(idplus)+len(idminus)
	eigenvalues = numpy.concatenate((eigenvaluesp,eigenvaluesm))

	vectorsBH = numpy.hstack([vectorsBHp, vectorsBHm])

	ids = numpy.argsort(eigenvalues) 
	
	if nCommBH <= q:
		vectorsBH = vectorsBH.take(ids[:nCommBH],axis=1)
	else:
		vectorsBH = vectorsBH.take(ids[:q],axis=1)

	vectorsBH = normalize(vectorsBH, norm='l2', axis=0)

	eigenvalues = [eigenvalues[i] for i in ids]

	centroids,_ = kmeans(vectorsBH,nCommBH,1000)
	inferred_sigma,_ = vq(vectorsBH, centroids)
	return inferred_sigma

# Initial (simple) commandline parsing
if len(sys.argv) < 3:
	print("python bp.py <file> <num_clusters> [<true partition file>]")
	exit()

f = open(sys.argv[1],'r')
num_clusters = int(sys.argv[2])

lines = f.readlines()
edges = []
maxNode = 0
for i in xrange(len(lines)):
	if i!=-1:
		line = lines[i].split()
		edges.append((int(line[0]),int(line[1])))

start = time.time()
inferred_sigma = betheHessianCluster(makeAdjMatrix(edges), computeNumNodes(edges), num_clusters)
end = time.time()
print(end-start)
print inferred_sigma
partition = {}
for i,val in enumerate(inferred_sigma):
	if val not in partition:
		partition[val] =[]
	partition[val].append(i)

GroundTruth = None
if len(sys.argv) > 3:
	f = open(sys.argv[3])
	vals = [int(line.strip()) for line in f]
	GroundTruth = {}
	for i in xrange(len(vals)):
		assignment = vals[i]
		if not assignment in GroundTruth: GroundTruth[assignment] = []
		GroundTruth[assignment].append(i)
	# Compute overlap
	print "NMI:", NMI(GroundTruth,partition)
	print "rNMI:", rNMI(GroundTruth,partition)
	print "overlap:", overlap(GroundTruth,partition)

f = open('tmp.txt', 'w')
for i in inferred_sigma:
	f.write(str(int(i))+'\t')