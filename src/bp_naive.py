##############################################################################
# This program reads in a file given on the commandline
# and performs belief propogation to detect communities.
# We assume the graph is undirected and given by edges
# formated as src_node, dst_node.
#
# This version is the naive version which is implemented
# in a straight forward manner. We use it as reference
# to check our other implementations.
##############################################################################


import snap
import sys
import random
import math
import time



##############################################################################
# Parse commannd line
if len(sys.argv) < 5:
	print("python bp_naive.py <file_name> <num_clusters> <beta> <verbose>")
	exit()
num_clusters = int(sys.argv[2])
beta = float(sys.argv[3])
verbose = int(sys.argv[4])

# File reading and parsing
graph = snap.TUNGraph.New()
if verbose: print("opening file "+sys.argv[1])
f = open(sys.argv[1])
node_list = []
M = 0 # number of edges
f.readline() # skip header line
for line in f:
	splitted = line.split()
	src_node = int(splitted[0])
	dst_node = int(splitted[1])
	if not src_node in node_list:
		graph.AddNode(src_node)
		node_list.append(src_node)
	if not dst_node in node_list:
		graph.AddNode(dst_node)
		node_list.append(dst_node)
	graph.AddEdge(src_node,dst_node)
	M += 1
if verbose: print("done creating graph")
##############################################################################



##############################################################################
# Perform initialization. This consists of:
#   allocating space for holding computed probabilities
#   randomly initializing probabilities of node belonging to each group
#   create dictionary with random messages

# Contains the probabilities of each node belonging to each cluster
# node_community_probs[node_id][cluster] = <some prob>
node_community_probs = []
# Contains the messages as dictionary
# Indexed like messages[(from,to)]
messages = {}
N = len(node_list) # number of nodes
for i in range(N):
	node_probs = [random.random() for k in range(num_clusters)] # set probabilites randomly
	total = sum(node_probs)
	node_probs = [val/total for val in node_probs] # normalize to sum 1
	node_community_probs.append(node_probs)
	neighbors = snap.TIntV()
	snap.GetNodesAtHop(graph, i, 1, neighbors, False)
	for neighbor in neighbors:
		new_messages = [random.random() for k in range(num_clusters)] # initialize messages
		total = sum(new_messages)
		new_messages = [val/total for val in new_messages] # normalize to sum 1
		messages[(i,neighbor)] = new_messages
##############################################################################



##############################################################################
# Run the algorithm outlined as follows:
#   compute common data used in all the messages
#   compute all messages
#   perform updates of all the nodes
#   renormalize the probabilities
#while not_converged:
start = time.time()
for iteration in range(10):
	if verbose: print(iteration)
	# First compute thedas, some common data representing the field
	thetas = [0]*num_clusters
	for node in graph.Nodes():
		node_idx = node.GetId()
		degree = node.GetOutDeg()
		for t in range(num_clusters): thetas[t] += degree*node_community_probs[node_idx][t]
	# Compute messages for each node
	# Note indexers are chosen to match the paper
	new_msgs_all = {}
	for i in range(N):
		neighbors = snap.TIntV()
		snap.GetNodesAtHop(graph, i, 1, neighbors, False)
		d_i = len(neighbors) # degree of node i
		for k in neighbors:
			vals = []
			for t in range(num_clusters):
				total = 0
				for j in neighbors:
					if k == j: continue
					total += math.log(1+(math.exp(beta)-1)*messages[(j,i)][t])
				val = math.exp(-1*beta*d_i*thetas[t]/float(2)/float(M) + total)
				vals.append(val)
			# Normalize vals
			vals = [val/sum(vals) for val in vals]
			new_msgs_all[(i,k)] = vals
	# All new messages are computed at this point
	# Now we should update the marginals
	for i in range(N):
		margs = []
		neighbors = snap.TIntV()
		snap.GetNodesAtHop(graph, i, 1, neighbors, False)
		d_i = len(neighbors) # degree of node i
		for t in range(num_clusters):
			total = 0
			for j in neighbors:
				total += math.log(1+(math.exp(beta)-1)*messages[(j,i)][t])
			val = math.exp(-1*beta*d_i*thetas[t]/float(2)/float(M) + total)
			margs.append(val)
		margs = [val/sum(margs) for val in margs]
		node_community_probs[i] = margs
	messages = new_msgs_all
	if verbose:
		print("--------------------------------------------------------------")
		print("iteration " + str(iteration))
		for i in range(N):
			print_str = str(i) +": "
			print_str += str(node_community_probs[i].index(max(node_community_probs[i])))
			print_str += ": "+str(node_community_probs[i])
			print(print_str)
		print("--------------------------------------------------------------")
end = time.time()
print("--------------------------------------------------------------")
print("final results")
assignments = []
for i in range(N):
	print_str = str(i) +": "
	assignment = node_community_probs[i].index(max(node_community_probs[i]))
	assignments.append(assignment)
	print_str += str(assignment)
	print_str += ": "+str(node_community_probs[i])
	print(print_str)
print("--------------------------------------------------------------")
print("time (excluding loading): " + str(end-start))
##############################################################################



##############################################################################
# Compute modularity of our partitions
if False:
	degrees = [0]*N
	for node in graph.Nodes():
		degrees[node.GetId()] = node.GetOutDeg()
	total = 0
	for edge in graph.Edges():
		src = edge.GetSrcNId()
		dst = edge.GetDstNId()
		if assignments[src] == assignments[dst]:
			total += 1 - degrees[src]*degrees[dst]/float(2)/float(M)
	print(total/float(M))
##############################################################################
