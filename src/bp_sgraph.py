from sframe import SGraph, SFrame, Vertex, Edge
import random
import math
import sys

from sframe.aggregate import SUM

###############################################################################
# Initial (simple) commandline parsing
if len(sys.argv) < 5:
	print("python bp.py <file> <num_clusters> <beta> <verbose>")
	exit()

num_clusters = int(sys.argv[2])
beta = float(sys.argv[3])
verbose = int(sys.argv[4])
###############################################################################



###############################################################################
# Initialize a sgraph from the file
# Read edges from file
edge_frame = SFrame()
edge_frame = edge_frame.read_csv(sys.argv[1],"\t",True)
edge_frame = edge_frame.unique() # May need later
g = SGraph().add_edges(edge_frame,"src","dst")

# Get the number of edges
num_edges = g.edges.shape[0]

# Make each edge bidirectional by adding the other direction
# Necessary since messages in either direction are different
f2 = g.edges.copy()
f2["__src_id"] = g.edges["__dst_id"]
f2["__dst_id"] = g.edges["__src_id"]
g = g.add_edges(f2,"__src_id","__dst_id")

# Two functions for initialization
def random_msg_init(src,edge,dst):
	global num_clusters
	vals = [random.random() for t in range(num_clusters)]
	total = sum(vals)
	vals = [val/total for val in vals]
	dst['message'][src['__id']] = vals
	return (src,edge,dst)
def random_marg_init(node):
	vals = [random.random() for t in range(num_clusters)]
	total = sum(vals)
	vals = [val/total for val in vals]
	return vals

# Initialize messages between all nodes
g.vertices['message'] = g.vertices['__id'].apply(lambda x: {})
g = g.triple_apply(random_msg_init,['message'])
# Add marginals attribute to nodes
g.vertices["marginals"] = g.vertices["__id"].apply(random_marg_init)

# Add edges in both direction
if verbose:
	print("VERTICES: ")
	print(g.vertices)
	print("EDGES: ")
	g.edges.print_rows(g.edges.shape[0])
###############################################################################



###############################################################################
# Create needed functions for message passing
def compute_degree(src,edge,dst):
	src['degree'] += 1
	return (src,edge,dst)

def compute_outgoing_messages(src,edge,dst):
	global beta
	global num_clusters
	global num_edges
	global theta
	out_going_msg = []
	for t in range(num_clusters):
		val = -1*beta*src['degree']*theta[t]/float(2)/float(num_edges)
		for key in src['message']:
			if key == dst['__id']: continue
			val += math.log(1 + (math.exp(beta)-1)*src['message'][key][t])
		out_going_msg.append(math.exp(val))
	out_going_msg = [val/sum(out_going_msg) for val in out_going_msg]
	dst['new_message'][src['__id']] = out_going_msg
	return (src,edge,dst)

def compute_node_marginals(node):
	global beta
	global num_clusters
	global num_edges
	global theta
	new_margs = []
	for t in range(num_clusters):
		val = -1*beta*node['degree']*theta[t]/float(2)/float(num_edges)
		for key in node['message']:
			val += math.log(1 + (math.exp(beta)-1)*node['message'][key][t])
		new_margs.append(math.exp(val))
	new_margs = [val/sum(new_margs) for val in new_margs]
	return new_margs

def get_theta_t(node):
	global t
	return node['degree']*node['marginals'][t]
###############################################################################



###############################################################################
# Use functions to run bp
# First compute degree for each node
g.vertices['degree'] = 0
g = g.triple_apply(compute_degree,['degree'])

# As initialization compute incoming messages and theta
theta = [0]*num_clusters

for iteration in range(20):
	print(iteration)

	# Now we update fields (theta) using marginals
	print('compute theta')
	for t in range(num_clusters):
		theta[t] = g.vertices.apply(get_theta_t).sum()

	# Now update marginals using new messages
	print('compute marginals')
	del g.vertices['marginals']
	g.vertices['marginals'] = g.vertices.apply(compute_node_marginals)

	# Compute outgoing messages
	print('compute messages')
	g.vertices['new_message'] = g.vertices["__id"].apply(lambda x: {})
	g = g.triple_apply(compute_outgoing_messages,['new_message'])

	print('move data')
	g.vertices['message'] = g.vertices['new_message']
	del g.vertices['new_message']

# Remove temp data from graph
del g.vertices['degree']
del g.vertices['message']
# And add the community assignment
g.vertices['assignment'] = g.vertices['marginals'].apply(lambda x: x.index(max(x)))

g.vertices.print_rows(g.vertices.shape[0])
###############################################################################
