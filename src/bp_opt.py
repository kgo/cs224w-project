##############################################################################
# This program reads in a file given on the commandline
# and performs belief propogation to detect communities.
# We assume the graph is undirected and given by edges
# formated as src_node, dst_node.
#
# This version is the naive version which is implemented
# in a straight forward manner. We use it as reference
# to check our other implementations.
##############################################################################


import sys
import random
import math
import time

import os
sys.path.append(os.getcwd())
from nmi import NMI,rNMI


##############################################################################
# Define a class for nodes, a graph will be an array of such
class Node:
	def __init__(self,id_):
		self.id = id_
		# Dictionary mapping nodes to indices (used to index messages)
		self.neighbors = {}
		self.neighbor_ptrs = []
	def AddEdge(self,node,bidirectional=True):
		if node.id in self.neighbors.keys(): return
		self.neighbors[node.id] = len(self.neighbors)
		self.neighbor_ptrs.append(node)
		if bidirectional: node.AddEdge(self,False)
	def GetMessageFor(self, node):
		#	Get the message from self to node
		return self.messages[self.neighbors[node.id]]
	
	#	Normalize probabilites
	def NormalizeMarginals(self):
		sum_margs = sum(self.marginals)
		for k in xrange(len(self.marginals)): self.marginals[k] /= sum_margs
	def NormalizeMessages(self,num_clusters):
		m = self.messages
		for i in xrange(self.degree):
			sum_message = sum(self.messages[i])
			for k in xrange(num_clusters): m[i][k] /= sum_message
	
	# Initialze object for belief propogation
	# Make sure that all nodes and edges are added at this point
	def InitBP(self,num_clusters):
		self.degree = len(self.neighbors)
		self.messages = [[random.random() for i in xrange(num_clusters)]
												for j in xrange(self.degree)]
		self.marginals = [random.random() for i in xrange(num_clusters)]
		self.new_messages = [[0]*num_clusters for i in xrange(self.degree)]
		# Normalize the probabilities
		self.NormalizeMessages(num_clusters)
		self.NormalizeMarginals()
	
	# Copies messages from self.new_messages to self.messages
	# This version uses a deep copy, could also switch
	# to alternating between two buffers
	def CopyMessages(self,num_clusters):
		for i in xrange(self.degree):
			for k in xrange(num_clusters):
				self.messages[i][k] = self.new_messages[i][k]
		self.NormalizeMessages(num_clusters)
	
	# Compute some a function of each message
	# The message values are only used in this form
	# so this factors out some common
	def UpdateCommon(self,num_clusters,beta):
		for i in xrange(self.degree):
			for k in xrange(num_clusters):
				self.messages[i][k] = math.log(1+(math.exp(beta)-1)*self.messages[i][k])
	
	# Computes new messages as new field
	# You should call CopyMessages after you do this
	def UpdateMessages(self,thetas,num_clusters,beta,M):
		# Create list of neighbor messages (hopefully this doesn't deep copy)
		degree = self.degree
		incoming_messages = [None for i in xrange(self.degree)]
		totals_for_clusters = [0]*num_clusters
		for neighbor in self.neighbor_ptrs:
			neighbor_index = self.neighbors[neighbor.id]
			incoming_messages[neighbor_index] = neighbor.GetMessageFor(self)
			for t in xrange(num_clusters):
				totals_for_clusters[t] += incoming_messages[neighbor_index][t]
		for t in xrange(num_clusters):
			totals_for_clusters[t] = -1*beta*degree*thetas[t]/float(2*M) + totals_for_clusters[t]
		# Compute messages for all neighbors
		nm = self.new_messages
		for k in xrange(degree):
			for t in xrange(num_clusters): nm[k][t] = totals_for_clusters[t] - incoming_messages[k][t]
			max_val = max(nm[k])
			for t in xrange(num_clusters): nm[k][t] = nm[k][t]-max_val
			try:
				for t in xrange(num_clusters): nm[k][t] = math.exp(nm[k][t])
			except:
				print(nm[k])
				raise
		# Compute marginals
		max_val = max(totals_for_clusters)
		for t in xrange(num_clusters): self.marginals[t] = math.exp(totals_for_clusters[t]-max_val)
		self.NormalizeMarginals()

	def __str__(self):
		return_str = "Node " + str(self.id) + "\n"
		return_str += "\tneighbors: " + str(self.neighbors) + "\n"
		return_str += "\tmessages: " + str(self.messages) + "\n"
		return_str += "\tmarginals: " + str(self.marginals)
		return return_str

# Example of creating a small graph
"""
nodes = []
for i in xrange(5):
	nodes.append(Node(i))
	for j in xrange(i): nodes[i].AddEdge(nodes[j])
for i in xrange(5):
	nodes[i].InitBP(2)
	print(nodes[i])
exit()
"""
##############################################################################



##############################################################################
# Parse commannd line
if len(sys.argv) < 5:
	print("python bp_opt.py <file_name> <num_clusters> <beta> <verbose> [<truth>]")
	exit()
num_clusters = int(sys.argv[2])
beta = float(sys.argv[3])
verbose = int(sys.argv[4])

GroundTruth = None
if len(sys.argv) == 6:
	f = open(sys.argv[5])
	vals = [int(line.strip()) for line in f]
	GroundTruth = {}
	for i in xrange(len(vals)):
		assignment = vals[i]
		if not assignment in GroundTruth: GroundTruth[assignment] = []
		GroundTruth[assignment].append(i)
	print("GIVEN GROUND TRUTH")

# File reading and parsing
if verbose: print("opening file "+sys.argv[1])
f = open(sys.argv[1])
nodes = []
M = 0 # number of edges
f.readline() # skip header line
for line in f:
	splitted = line.split()
	src_node = int(splitted[0])
	dst_node = int(splitted[1])
	# Create nodes up to the needed index
	max_index = max(src_node,dst_node)
	while max_index >= len(nodes): nodes.append(Node(len(nodes)))
	# Now create edge (i.e. update the neighbors list)
	nodes[src_node].AddEdge(nodes[dst_node],True)
	M += 1
num_nodes = len(nodes)
if verbose: print("done creating graph")
##############################################################################



##############################################################################
# Perform initialization. This consists of:
#   allocating space for holding computed probabilities
#   randomly initializing probabilities of node belonging to each group
#   randomly initializing the messages
# This is all handled by the class method
for i in xrange(num_nodes): nodes[i].InitBP(num_clusters)
if verbose: print("done node initialization")
##############################################################################



##############################################################################
# Run the algorithm outlined as follows:
#   compute common data used in all the messages
#   compute all messages
#   perform updates of all the nodes
#   renormalize the probabilities
#while not_converged:
# Phases show sections that should be computed serially
# The loops can be computed in parallel
num_nodes = len(nodes)
start = time.time()
nmi = []
rnmi = []
for iteration in xrange(10):
	if verbose: print(iteration)

	# Phase I: Compute some common data this 
	# First compute thedas, some common data representing the field
	if verbose: print("Phase I")
	thetas = [0]*num_clusters
	for i in xrange(num_nodes):
		degree = nodes[i].degree
		for t in xrange(num_clusters): thetas[t] += degree*nodes[i].marginals[t]

	# Update some common values
	for i in xrange(num_nodes): nodes[i].UpdateCommon(num_clusters,beta)

	# Phase II: Compute new variables for next state
	# Update the marginals for each node
	if verbose: print("Phase II")
	for i in xrange(num_nodes): nodes[i].UpdateMessages(thetas,num_clusters,beta,M)
	
	# Phase III: Copy data over
	# Update nodes now that all new messages have been computed
	if verbose: print("Phase III")
	for i in xrange(num_nodes): nodes[i].CopyMessages(num_clusters)

	if not GroundTruth is None:
		# Compute NMI and rNMI
		clusters = {i:[] for i in xrange(num_clusters)}
		for i in xrange(num_nodes):
			ni_marginals = nodes[i].marginals
			assignment = ni_marginals.index(max(ni_marginals))
			clusters[assignment].append(i)
		nmi.append(NMI(GroundTruth,clusters))
		rnmi.append(rNMI(GroundTruth,clusters))

	#	Compute modularity
	if False:
		assignments = [0]*num_nodes
		for i in xrange(num_nodes):
			ni_marginals = nodes[i].marginals
			assignments[i] = ni_marginals.index(max(ni_marginals))
		total = 0
		for node in nodes:
			src = node.id
			for neighbor in node.neighbor_ptrs:
				dst = neighbor.id
				# Compute for each edge once
				if src < dst and assignments[src] == assignments[dst]:
					total += 1 - node.degree*neighbor.degree/float(2*M)
		print("modularity" + str(total/float(M)))

	"""
	if verbose:
		print("--------------------------------------------------------------")
		print("iteration " + str(iteration))
		for i in xrange(num_nodes):
			ni_marginals = nodes[i].marginals
			print_str = str(i) +": "
			print_str += str(ni_marginals.index(max(ni_marginals)))
			print_str += ": "+str(ni_marginals)
			print(print_str)
		print("--------------------------------------------------------------")
	"""
end = time.time()
print("--------------------------------------------------------------")
print("final results")
assignments = []
cluster_to_nodes = {i:[] for i in xrange(num_clusters)}
for i in xrange(num_nodes):
	ni_marginals = nodes[i].marginals
	assignment = ni_marginals.index(max(ni_marginals))
	assignments.append(assignment)
	cluster_to_nodes[assignment].append(i)
	print_str = str(i) +": "
	print_str += str(assignment)
	print_str += ": "+str(ni_marginals)
	#print(print_str)
for i in cluster_to_nodes:
	cluster = cluster_to_nodes[i]
	#print(cluster)
	print(cluster)
	#print(len(cluster))
if not GroundTruth is None: print(GroundTruth)
print("--------------------------------------------------------------")
print(end-start)
#print(assignments)
f = open('temp','w+')
f.write(str(assignments))
f.close()

if not GroundTruth is None:
	print(nmi)
	print(rnmi)
##############################################################################
