##############################################################################
# This program reads in a file given on the commandline
# and performs belief propogation to detect communities.
# We assume the graph is undirected and given by edges
# formated as src_node, dst_node.
#
# This version is the naive version which is implemented
# in a straight forward manner. We use it as reference
# to check our other implementations.
##############################################################################


import sys
import random
import math
import multiprocessing
from multiprocessing.managers import BaseManager


##############################################################################
# Define a class for nodes, a graph will be an array of such
class Node:
	def __init__(self,id_):
		self.id = id_
		# Dictionary mapping nodes to indices (used to index messages)
		self.neighbors = {}
		self.neighbor_ptrs = []
	def AddEdge(self,node,bidirectional=True):
		self.neighbors[node.id] = len(self.neighbors)
		self.neighbor_ptrs.append(node)
		if bidirectional: node.AddEdge(self,False)
	def GetDegree(self):
		return len(self.neighbors)
	def GetMessageFor(self, node):
		#	Get the message from self to node
		return self.messages[self.neighbors[node.id]]
	
	#	Normalize probabilites
	def NormalizeMarginals(self):
		sum_margs = sum(self.marginals)
		for k in range(len(self.marginals)): self.marginals[k] /= sum_margs
	def NormalizeMessages(self,num_clusters):
		for i in range(self.GetDegree()):
			sum_message = sum(self.messages[i])
			for k in range(num_clusters): self.messages[i][k] /= sum_message
	
	# Initialze object for belief propogation
	def InitBP(self,num_clusters):
		self.messages = [[random.random() for i in range(num_clusters)]
												for j in range(len(self.neighbors))]
		self.marginals = [random.random() for i in range(num_clusters)]
		self.new_messages = [[0]*num_clusters for i in range(self.GetDegree())]
		# Normalize the probabilities
		self.NormalizeMessages(num_clusters)
		self.NormalizeMarginals()
	
	# Copies messages from self.new_messages to self.messages
	# This version uses a deep copy, could also switch
	# to alternating between two buffers
	def CopyMessages(self,num_clusters):
		for i in range(self.GetDegree()):
			for k in range(num_clusters):
				self.messages[i][k] = self.new_messages[i][k]
		self.NormalizeMessages(num_clusters)
	
	# Compute some a function of each message
	# The message values are only used in this form
	# so this factors out some common
	def UpdateCommon(self,num_clusters,beta):
		for i in range(self.GetDegree()):
			for k in range(num_clusters):
				self.messages[i][k] = math.log(1+(math.exp(beta)-1)*self.messages[i][k])
	
	# Computes new messages as new field
	# You should call CopyMessages after you do this
	def UpdateMessages(self,thetas,num_clusters,beta,M):
		# Create list of neighbor messages (hopefully this doesn't deep copy)
		degree = self.GetDegree()
		incoming_messages = [None for i in range(self.GetDegree())]
		totals_for_clusters = [0]*num_clusters
		for neighbor in self.neighbor_ptrs:
			neighbor_index = self.neighbors[neighbor.id]
			incoming_messages[neighbor_index] = neighbor.GetMessageFor(self)
			for t in range(num_clusters):
				totals_for_clusters[t] += incoming_messages[neighbor_index][t]
		# Compute messages for all neighbors
		for k in range(degree):
			for t in range(num_clusters):
				val = -1*beta*degree*thetas[t]/float(2*M)
				val += totals_for_clusters[t] - incoming_messages[k][t]
				self.new_messages[k][t] = math.exp(val)
		# Compute marginals
		for t in range(num_clusters):
			val = -1*beta*degree*thetas[t]/float(2*M) + totals_for_clusters[t]
			self.marginals[t] = math.exp(val)
		self.NormalizeMarginals()

	def __str__(self):
		return_str = "Node " + str(self.id) + "\n"
		return_str += "\tneighbors: " + str(self.neighbors) + "\n"
		return_str += "\tmessages: " + str(self.messages) + "\n"
		return_str += "\tmarginals: " + str(self.marginals)
		return return_str

# Example of creating a small graph
"""
nodes = []
for i in range(5):
	nodes.append(Node(i))
	for j in range(i): nodes[i].AddEdge(nodes[j])
for i in range(5):
	nodes[i].InitBP(2)
	print(nodes[i])
exit()
"""
##############################################################################



##############################################################################
# Function and classes for parallelism
class NodeList(object):
	def __init__(self):
		self.nodes = []
	def addNode(self,i):
		self.nodes.append(Node(i))
	def addEdge(self,src,dst):
		self.nodes[src].AddEdge(self.nodes[dst],True)
	def UpdateCommon(self,i,num_clusters,beta):
		self.nodes[i].UpdateCommon(num_clusters,beta)
	def UpdateMessages(self,i,thetas,num_clusters,beta,M):
		self.nodes[i].UpdateMessages(thetas,num_clusters,beta,M)
	def CopyMessages(self,i,num_clusters):
		self.nodes[i].CopyMessages(num_clusters)
	def NumNodes(self): return len(self.nodes)
	def InitBP(self,num_clusters):
		for i in range(len(self.nodes)): self.nodes[i].InitBP(num_clusters)
	def ComputeThetas(self,num_clusters,thetas):
		for i in range(len(self.nodes)):
			degree = self.nodes[i].GetDegree()
			for t in range(num_clusters): thetas[t] += degree*self.nodes[i].marginals[t]
	def GetMarginals(self,i):
		return self.nodes[i].marginals
		

class MyManager(BaseManager): pass

MyManager.register('NodeList', NodeList)
##############################################################################



##############################################################################
# Define the functions to be run by threads for parallel execution
def threadUpdateCommon(start,end,nodes,num_clusters,beta):
	for i in range(start,end): nodes.UpdateCommon(i,num_clusters,beta)

def threadUpdateMessages(start,end,nodes,thetas,num_clusters,beta,M):
	for i in range(start,end):
		nodes.UpdateMessages(i,thetas,num_clusters,beta,M)

def threadCopyMessages(start,end,nodes,num_clusters):
	for i in range(start,end): nodes.CopyMessages(i,num_clusters)
##############################################################################



##############################################################################
# Parse commannd line
if __name__ == '__main__':
	if len(sys.argv) < 5:
		print("python bp_parallel.py <file_name> <num_clusters> <beta> <verbose>")
		exit()
	num_clusters = int(sys.argv[2])
	beta = float(sys.argv[3])
	verbose = int(sys.argv[4])

	# File reading and parsing
	print("opening file "+sys.argv[1])
	f = open(sys.argv[1])

	# Create parallel objects
	manager = MyManager()
	manager.start()
	nodes = manager.NodeList()
	pool = multiprocessing.Pool(multiprocessing.cpu_count())

	M = 0 # number of edges
	f.readline() # skip header line
	for line in f:
		splitted = line.split()
		src_node = int(splitted[0])
		dst_node = int(splitted[1])
		# Create nodes up to the needed index
		max_index = max(src_node,dst_node)
		while max_index >= nodes.NumNodes(): nodes.addNode(nodes.NumNodes())
		# Now create edge (i.e. update the neighbors list)
		nodes.addEdge(src_node,dst_node)
		M += 1
	num_nodes = nodes.NumNodes()
	print("done creating graph")
##############################################################################



##############################################################################
# Perform initialization. This consists of:
#   allocating space for holding computed probabilities
#   randomly initializing probabilities of node belonging to each group
#   randomly initializing the messages
# This is all handled by the class method
	nodes.InitBP(num_clusters)
	print("done node initialization")
##############################################################################



##############################################################################
# Run the algorithm outlined as follows:
#   compute common data used in all the messages
#   compute all messages
#   perform updates of all the nodes
#   renormalize the probabilities
#while not_converged:
# Phases show sections that should be computed serially
# The loops can be computed in parallel
	num_threads = multiprocessing.cpu_count()
	block_size = nodes.NumNodes()/num_threads
	results = []
	for iteration in range(20):
		print(iteration)

		# Phase I: Compute some common data this 
		# First compute thedas, some common data representing the field
		if verbose: print("Phase I")
		thetas = [0]*num_clusters
		nodes.ComputeThetas(num_clusters,thetas)

		# Update some common values
		for t in range(num_threads):
			upper = min(num_nodes,(t+1)*block_size)
			result = pool.apply_async(func=threadUpdateCommon,
								args=(t*block_size,upper,nodes,num_clusters,beta))
			results.append(result)
		for result in results: result.wait()
		del results[:]

		# Phase II: Compute new variables for next state
		# Update the marginals for each node
		if verbose: print("Phase II")
		for t in range(num_threads):
			upper = min(num_nodes,(t+1)*block_size)
			result = pool.apply_async(func=threadUpdateMessages,
								args=(t*block_size,upper,nodes,thetas,num_clusters,beta,M))
			results.append(result)
		for result in results: result.wait()
		del results[:]
		
		# Phase III: Copy data over
		# Update nodes now that all new messages have been computed
		if verbose: print("Phase III")
		for t in range(num_threads):
			upper = min(num_nodes,(t+1)*block_size)
			result = pool.apply_async(func=threadCopyMessages,
								args=(t*block_size,upper,nodes,num_clusters))
			results.append(result)
		for result in results: result.wait()
		del results[:]

		"""
		if verbose:
			print("--------------------------------------------------------------")
			print("iteration " + str(iteration))
			for i in range(num_nodes):
				ni_marginals = nodes[i].marginals
				print_str = str(i) +": "
				print_str += str(ni_marginals.index(max(ni_marginals)))
				print_str += ": "+str(ni_marginals)
				print(print_str)
			print("--------------------------------------------------------------")
		"""
	print("--------------------------------------------------------------")
	print("final results")
	assignments = []
	for i in range(num_nodes):
		ni_marginals = nodes.GetMarginals(i)
		assignment = ni_marginals.index(max(ni_marginals))
		assignments.append(assignment)
		print_str = str(i) +": "
		print_str += str(assignment)
		print_str += ": "+str(ni_marginals)
		print(print_str)
	print("--------------------------------------------------------------")
##############################################################################
