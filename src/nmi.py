###############################################################################
# This file defines NMI and rNMI to use for comparing partitions
###############################################################################

from __future__ import division
import math
import random
import numpy
from itertools import permutations
from munkres import Munkres

def find(a, func):
    return [i for (i, val) in numpy.ndenumerate(a) if func(val)]

def NMI(partitionA, partitionB):
	NA = sum([len(i) for i in partitionA.values()])
	NB = sum([len(i) for i in partitionB.values()])
	assert NA == NB
	entropyA = 0
	entropyB = 0
	I = 0
	for i in partitionA.values():
		n = len(i)/NA
		entropyA -= n*math.log(n)
	for i in partitionB.values():
		n = len(i)/NB
		entropyB -= n*math.log(n)
	for a in partitionA.values():
		for b in partitionB.values():
			num = len(set(a) & set(b))
			if num > 0:
				I += num/NA * math.log(NA*num/len(a) / len(b))
	return 2*I/(entropyA+entropyB)

def rNMI(partitionA, partitionB, numIter = 10):
	NA = sum([len(i) for i in partitionA.values()])
	NB = sum([len(i) for i in partitionB.values()])
	assert NA == NB
	totalNMI = 0
	for i in xrange(numIter):
		shuffled = range(NB)
		random.shuffle(shuffled)
		partitionC = {}
		idx = 0
		for key,value in partitionB.iteritems():
			partitionC[key] = shuffled[idx:idx+len(value)]
			idx+=len(value)
		totalNMI += NMI(partitionA, partitionC)
	totalNMI /= numIter
	return NMI(partitionA,partitionB) - totalNMI

def overlap(partitionA, partitionB):
	NA = sum([len(i) for i in partitionA.values()])
	NB = sum([len(i) for i in partitionB.values()])

	assert NA == NB
	overlap = 0
	costMat = numpy.zeros((len(partitionA), len(partitionA)))
	vecA = [0]*NA
	vecB = [0]*NB
	for i,val in partitionA.iteritems():
		for j in val:
			vecA[j] = i

	for i,val in partitionB.iteritems():
		for j in val:
			vecB[j] = i	

	for a in xrange(len(partitionA)):
		for b in xrange(len(partitionA)):
			idx = find(vecB, lambda x: x==a)
			tmp = [vecA[i[0]] for i in idx]
			costMat[a,b] = -(len(find(tmp, lambda x: x==b)))**2

	munk = Munkres()
	indexes = munk.compute(costMat)
	bToA = {}
	for i in indexes:
		bToA[i[0]] = i[1]
	for i in xrange(len(partitionB)):
		overlap += len(set(partitionA[bToA[i]]).intersection(set(partitionB[i]))) / NA
 # 	for p in permutations(partitionA.keys()):
	# 	overlap = 0
	# 	for i in xrange(len(p)):
	# 		if i < len(partitionB):
	# 			overlap += len(set(partitionA[p[i]]).intersection(set(partitionB[partitionB.keys()[i]]))) / NA
	# 	if overlap > max_overlap:
	# 		max_overlap = overlap
	# print max_overlap
	print overlap
	return (overlap - 1/len(partitionA))/ (1-1/len(partitionA))
