import sys
import networkx as nx

if len(sys.argv) < 4:
    print "Usage: %s <input gml file> <membership file> <output gml file>" % sys.argv[0]
    quit()

g = nx.read_gml(sys.argv[1])

def getMembership(fname):
    text_file = open(fname, "r")
    membership = {}
    for i,val in enumerate(text_file.read().split()):
    	membership[str(i)] = val
    return membership
membership = getMembership(sys.argv[2])

nx.set_node_attributes(g, 'partition', membership)

nx.write_gml(g,sys.argv[3])