import igraph, sys
from random import randint

def getMembership(fname):
    text_file = open(fname, "r")
    return [int(i) for i in text_file.read().split()]


def _plot(g, membership=None):
    if membership is not None:
        gcopy = g.copy()
        edges = []
        edges_colors = []
        for edge in g.es():
            if membership[edge.tuple[0]] != membership[edge.tuple[1]]:
                edges.append(edge)
                edges_colors.append("rgba(100,100,100,0.3)")
            else:
                edges_colors.append("rgba(0,0,0,0.3)")
        gcopy.delete_edges(edges)
        layout = gcopy.layout("kk")
        g.es["color"] = edges_colors
    else:
        layout = g.layout("kk")
        g.es["color"] = "rgba(100,100,100,0.3)"
    visual_style = {}
    visual_style["vertex_label_dist"] = 0
    visual_style["vertex_shape"] = "circle"
    visual_style["edge_color"] = g.es["color"]
    visual_style["vertex_size"] = 30
    visual_style["layout"] = layout
    visual_style["bbox"] = (1920,1080)
    visual_style["margin"] = 40
    # visual_style["edge_label"] = g.es["weight"]
    for vertex in g.vs():
        vertex["label"] = vertex.index
    if membership is not None:
        colors = []
        colors.append('%06X' % 0xd11141)
        colors.append('%06X' % 0x00b159)
        colors.append('%06X' % 0x00aedb)
        colors.append('%06X' % 0xf37735)
        colors.append('%06X' % 0xffc425)
        for i in range(len(colors), max(membership)+1):
            colors.append('%06X' % randint(0, 0xFFFFFF))
        for vertex in g.vs():
            vertex["color"] = str('#') + colors[membership[vertex.index]]
        visual_style["vertex_color"] = g.vs["color"]
    igraph.plot(g, **visual_style)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s <edge list> <membership vector>" % sys.argv[0]
        quit()
    membership = getMembership(sys.argv[2])
    g = igraph.Graph()
    g.add_vertices(len(membership))
    f = open(sys.argv[1])
    f.readline() # skip header line
    edges = []
    for line in f:
        splitted = line.split()
        src_node = int(splitted[0])
        dst_node = int(splitted[1])
        edges.append((src_node, dst_node))
    g.add_edges(edges)
    _plot(g, membership)