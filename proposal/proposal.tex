\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1.25in]{geometry}
\usepackage{url}
\newcommand{\ie}{{\it i.e.}}


\title{Statistical Physics of Community Detection}
\author{Kenji Hata (khata), Keegan Go (keegango)}
\date{October 15, 2015}

\begin{document}

\maketitle

\section{Introduction}
Community detection is a key problem in network science. The goal is to
identify communities, defined as densely connected groups of nodes with
relatively fewer outgoing edges. Finding such communities can inform properties
of nodes and give insight into the nature of the network. However, current
methods often fail to reproduce groupings associated with recorded graph
metadata on real-world networks.

\section{Literature Survey}

\subsection{Definitions}

These are the relevant definitions that we learned about through our literature search.

\begin{itemize}
\item Community: a partition of nodes in a network that is densely interconnected
\item Partition: a grouping of nodes (can be overlapping, but will be noted when such)
\item Modularity: the attribute of a partition being densely interconnected but
having sparse connections to nodes outside the partition. It is defined as
\[
Q(\{t\}) = \frac{1}{m}\left(\sum_{\langle ij \rangle \in E} \delta_{t_it_j} - \sum_{\langle ij \rangle \in E} \frac{d_id_j}{2m} \delta_{t_it_j}\right).
\]
$\{t\}$ is our partition given as a vector with entries $1,\ldots,q$ with $q$
is the number of partitions. $E$ is the set of edges which has size $m$, and
$d_i$ is the degree of node $i$. This function essentially captures the number
of edges that lie within some group of the partition, and the expected number
of edges.
\item Normalized Mutual Information (NMI): a measure of the similarity of two partitions
\item Metadata groups: annotations for a graph which give the true communities.
These are obtained by external knowledge of the network, or by interviewing the
subjects.
\item Stochastic Block Model (SBM): a common generative model used to evaluate community detection algorithms.  
\end{itemize}

Hric et al., in \cite{hric}, set the stage with a detailed analysis of how, for
many networks, discovered partitions often fail to align with the metadata
groups. In their work, the authors used a variety of (mostly) greedy methods
for community discovery. These algorithms take advantage of local structure,
	often in the form of growing clusters of nodes or edges to form communities.
	The algorithms were tested on diverse datasets ranging from the Zachary's
	karate club network (34 nodes, 78 edges) to the Live Journal dataset(5
	million nodes, 50 million edges) \cite{mislove}. What was found was that the
	goodness of the community detection was more a function of the graph rather
	than the algorithm used. This, to the authors, suggest an incompatibility
	between our measures of communities (\ie, modularity, local link structure)
	and the actually properties of the ground truth groups that we'd like to be
	able to uncover. Overall, this paper served as a broad introduction to the
	relevant datasets and algorithms for community detection.
\\\\
Recently, new community discovering algorithms have been developed.  Saade et
al. \cite{saade2014spectral} focused on improving performance of spectral
clustering on sparse networks by introducing the Bethe Hessian operator. They
argue that traditional methods involving belief propagation (BP) are not
adequate, as the BP algorithm scales quadratically with the number of clusters
and needs to be given an accurate estimate of the SBM matrix. Furthermore,
these methods do not handle sparse networks well, as they fail to identify any
communities. However, spectral algorithms based on a non-backtracking walk of
directed edges has been used to rectify these problems and has been proven to
work well for SBM-generated graphs. The caveat is that this algorithm uses a
high-dimensional, non-symmetric matrix linear in the number of edges, resulting
in poor runtime efficiency. The Bethe Hessian operator aims to solve both
problems, as it is a symmetric, real matrix that is not parameter-dependent.
The Bethe Hessian operator is simply a regularization using the graph adjacency
matrix and the diagonal.  
\\\\
Another new method is given by Zhang and Moore in \cite{zhang2014scalable}. In
the paper, they describe a general problem with modularity based methods which
is that the maximum modularity over all partitions of a ER random networks is
often large even if the graph itself has no community structure. Their solution
is essentially to score community structure by the number of partitions with
large modularity. To this end they give a belief propagation (BP) algorithm to
compute these scores. They show that their algorithm, under certain parameter
settings, converges to a non-trivial partition, for SBM generated networks. On
real-world networks, the authors find their method detects partitions with
close similarity to ground truth (using overlap as a metric).


\section{Critique}
Hric et al. clearly demonstrate that the methods they tested do not do a good
job of predicting the ground truths for a variety of networks. However, they do
not offer much more than this, and do not provide clear next steps to pursue.
They suggest network topology is not sufficient to detect communities (at least
not in they way it is currently being used).
\\\\
In Saade et al. \cite{saade2014spectral}, the authors mention that the
non-backtracking walk of directed edges has proven to be optimal on SBM
networks, but state that their Bethe Hessian operator matches the performance
or works better on both SBM and real networks. However, the authors fail to
mention any deficiencies of the algorithm.  They also limit the real network
tests to only ones with metadata groups and fail to determine if their
algorithm can discover communities which previous algorithms have failed to
detect.
\\\\
Zhang's and Moore's (\cite{zhang2014scalable}) paper is perhaps the most
interesting. Their idea of using multiple partitions rather than just one with
the highest modularity makes sense as a way to make detection robust. Their
method is not without drawbacks. The BP is not guaranteed to converge, and
seems to have a high variance in steps to convergence, depending on whether you
are close to different convergence regimes. A follow up paper gives more detail
about this convergence \cite{schulke}, and the process is more nuanced than the
three regimes described by Zhang et al.

Another issue is that the authors only tested against ground truth using
relatively small real-world networks. In the large networks they tested, there
was no ground truth, but they point out that they found communities where
previous works said none existed. However, without a comparison, it is
difficult to say if this is a new find, or if their method also suffers from
the over-estimating modularity problem they sought to fix.

\section{Brainstorming}

The methods above often compare against a simple metric and claim to be
state-of-the-art, but we fail to gain true insight on how they perform when
compared against each other. 

Promising research directions include using these methods on larger networks,
testing against metadata groups for such larger networks, discovering new
communities in networks where none have been previously found, and applying
different metrics for comparison.  Hric et al. gives a table of existing
datasets with metadata groups, some of which are larger and have not been
tested by these algorithms. 

Another common theme among these papers is that the metrics do not accurately
reflect community structure. For example, maximum modularity is an insufficient
metric for community detection, as networks with no communities present may
produce a large maximum modularity. Thus, it may be possible that we will have
to come up with different metrics.

\section{Project Proposal}
For our project, we will implement and compare the Bethe Hessian
operator\cite{saade2014spectral} and a paralleled version of the BP community
detection algorithm in \cite{zhang2014scalable}. Our goal is to understand
community detection, and understand how well (or poorly) the current algorithms
perform on real world datasets.

\subsection{Methods and Models}
\cite{zhang2014scalable} contains a description of the algorithm. They also
provide a C++ implementation of the algorithm at \cite{modenet}. We believe we
can efficiently and scalable implement their method in Python, perhaps
exploiting parallelism, allowing us to see how their approach works on large
real data sets.

However, as suggested by \cite{saade2014spectral}, BP algorithms do not handle
sparse networks well. We will implement the Bethe Hessian operator and compare
it against how well \cite{zhang2014scalable}'s BP algorithm works.

To test our models during development, we plan to use both synthetic and real
networks. ER and SBM will be a core part of our testing as they are easy to
scale and configure. For real-networks, \cite{hric} lists a large number of
datasets that contain metadata, including

\begin{itemize}
\item karate
\item politic books/blogs
\item amazon
\item flickr
\item lj-mislove
\end{itemize}

These networks range in size in terms of nodes, edges, and groups allowing for
a diverse range of tests. Many are available online \cite{datasets}. The
largest graph that was tested on in \cite{zhang2014scalable} was approximately
one million nodes large and took nearly an hour to converge. That graph also
did not have associated metadata for comparison. We would like to be able to
test on even larger graphs, and see how the results compare to the ground truth
given to see if and what kinds of communities are detected.

\subsection{Evaluation of Results}
In order to evaluate how well our method works, we will look at the similarity
between detected partitions and the ground truth. There are a few ways to do
this. When comparing two partitions with equal, one can used overlap. However,
this does not easily generalize to comparing partitions with different numbers
of communities. To evaluate our partition in this case, we could use, we can
use the metric NMI. However, as suggested by Zhang in \cite{zhang2015revisit},
NMI is heavily affected by the network size and thus can give wrong estimates
of how well different algorithms perform. To combat this, we can use Zhang's
suggested relative NMI (rNMI) measure which accounts of the expected NMI of a
random partition. This is a relatively new metric, and so will hopefully give
us new insight into how well the discovered communities match the metadata
ones.

\subsection{Final Deliverable}
To summarize, we will implement a belief propagation community detector that is
scalable to large datasets, likely in Python. Using rNMI, we will evaluate our
detector on synthetic datasets to ensure correctness, and then run it on
real-world networks and see how closely we are able to predict the community
structure. Any remaining time can be spent tuning the algorithm for robustness,
as the BP algorithm shows notable areas of non-convergence.

\clearpage

\bibliographystyle{unsrt}
\bibliography{biblio}

\end{document}

